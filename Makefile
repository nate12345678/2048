CC=gcc
CFLAGS=-g -std=c99 -Wall #-Werror

all: mp8 

mp8: main.o game.o getch_fun.o
	$(CC) $(CFLAGS) main.o game.o getch_fun.o -o 2048

main.o: main.c
	$(CC) $(CFLAGS) -c main.c

game.o: game.c
	$(CC) $(CFLAGS) -c game.c

getch_fun.o: getch_fun.c
	$(CC) $(CFLAGS) -c getch_fun.c


clean:
	rm -f 2048 *.o *.a
