# 2048

To install, clone this repo, and run `make`.

To play, run the `2048` executable.

### Controls
w, a, s, d: move up, left, down, right

q: quit

n: restart/new game
